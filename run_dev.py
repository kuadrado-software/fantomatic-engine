from fantomatic_engine import run_game
import sys
import os

os.environ["EXEC_DIR"] = os.getcwd()

run_game(sys.argv[1:])
